CREATE TABLE IF NOT EXISTS project
(
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(128),
  description TEXT,
  create_time DATETIME,
  create_user_id INTEGER,
  update_time DATETIME,
  update_user_id INTEGER
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS issue
(
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(256) NOT NULL,
  description VARCHAR(2000),
  project_id INTEGER,
  type_id INTEGER,
  status_id INTEGER,
  owner_id INTEGER,
  requester_id INTEGER,
  create_time DATETIME,
  create_user_id INTEGER,
  update_time DATETIME,
  update_user_id INTEGER
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS user
(
  id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(256) NOT NULL,
  username VARCHAR(256),
  password VARCHAR(256),
  last_login_time Datetime,
  create_time DATETIME,
  create_user_id INTEGER,
  update_time DATETIME,
  update_user_id INTEGER
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS project_user_assignment
(
  project_id Int(11) NOT NULL,
  user_id Int(11) NOT NULL,
  create_time DATETIME,
  create_user_id INTEGER,
  update_time DATETIME,
  update_user_id INTEGER,
  PRIMARY KEY (project_id,user_id)
) ENGINE = InnoDB;

-- The Relationships
ALTER TABLE issue ADD CONSTRAINT FK_issue_project 
FOREIGN KEY (project_id) REFERENCES project (id) 
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE issue ADD CONSTRAINT FK_issue_owner 
FOREIGN KEY (owner_id) REFERENCES user (id) 
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE issue ADD CONSTRAINT FK_issue_requester 
FOREIGN KEY (requester_id) REFERENCES user (id) 
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE project_user_assignment ADD CONSTRAINT FK_project_user 
FOREIGN KEY (project_id) REFERENCES project (id) 
ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE project_user_assignment ADD CONSTRAINT FK_user_project 
FOREIGN KEY (user_id) REFERENCES user (id) 
ON DELETE CASCADE ON UPDATE RESTRICT;

-- Insert some seed data so we can just begin using the database

INSERT INTO user (email, username, password) VALUES
('user1@test.com','user1', MD5('user1')),
('user2@test.com','user2', MD5('user2'));

INSERT INTO project_user_assignment (project_id, user_id)
VALUES (1,1), (1,2);